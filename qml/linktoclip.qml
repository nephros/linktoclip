/*

Apache License 2.0

Copyright (c) 2022 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the
License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.Notifications 1.0
import "pages"
import "cover"

ApplicationWindow {
    id: app

    property bool gotUrlParm: false
    Notification { id: message; isTransient: true; }
    function popup(s) {
        message.previewSummary = s
        message.urgency = 0;
        message.publish();
    }
    ListModel{ id: args }
    Component.onCompleted: {
        // for sailjail
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Intialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.info("Parameters: " + Qt.application.arguments.join(" "))

        var p = Qt.application.arguments;
        for (var i=0;i<p.length;i++) {
            if (p[i] === "-u") {
                gotUrlParm = true;
                handleParameterItem(i,p[i+1]);
            }
        }
        if (!gotUrlParm) {
            console.warn("This should be called as: '<command> -u <URL>', no -u option or URL parameter detected");
            popup(qsTr("An error occurred: No URL detected!"));
        }
    }
    /*
     * fill the model
     * i = index of the parameter
     * p = url-like string
     */
    function handleParameterItem(i,p) {
        args.append( { "pid": i-1, "parameter": p, "desc": qsTr("Original Url"), "type": "original" } );
        /*
         * if we had a newer Qt, we could use
         *     var u = new URL(p);
         *     u.host, * u.protocol etc.
         *
         *     Alas, this apparently is only in Qt6, so we resort to RegExps here
         *     Still, here's a cheat sheet: (cf. https://javascript.info/url)
         *
         *
         *    |     origin                 |
         *    |         |    host          |
         *     https: // example.org : 8080 /path/to/page ?parm1=val1&parm2=val2 #frag0
         *    |      |  |           | |    |             |                      | 
         *    |  p   |  |     h     | |  p |      p      |          s           |   #
         *    |  r   |  |     o     | |  o |      a      |          e           |   h
         *    |  o   |  |     s     | |  r |      t      |          a           |   a
         *    |  t   |  |     t     | |  t |      h      |          r           |   s
         *    |  o   |  |     n     | |    |      n      |          c           |   h
         *    |  c   |  |     a     | |    |      a      |          h           | 
         *    |  o   |  |     m     | |    |      m      |                      | 
         *    |  l   |  |     e     | |    |      e      |                      | 
         *    |      |  |           | |    |             |                      | 
         */
        if (/^.+\/{2}/.test(p)) {
            const minusscheme = p.match(/^.+:\/*(.*)/)[1];
            const server = p.match(/^.+:\/{2}([^/]+)/)[1];
            const t = server.match(/^.+:([^/]+)/);
            const port = (t) ? t[1] : "-";
            const uri = minusscheme.match(/[^/]+(.*$)/)[1];
            const fragment = uri.substr(uri.indexOf("#"))
            const parameters = uri.substr(uri.indexOf("?")+1).replace(fragment,"")

            // without scheme
            args.append( { "pid": i-1, "parameter": minusscheme, "desc": qsTr("Without Protocol (scheme)"), "type": "scheme"} );
            // servername
            if (server !== "") args.append( { "pid": i-1, "parameter": server, "desc": qsTr("Server (local part)"), "type": "local" } );
            if (port !== "") args.append( { "pid": i-1, "parameter": port, "desc": qsTr("Port"), "type": "port" } );
            // uri/path
            if (uri !== "") args.append( { "pid": i-1, "parameter": uri, "desc": qsTr("URI string"), "type": "uri" } );
            if (parameters !== "") args.append( { "pid": i-1, "parameter": parameters, "desc": qsTr("URI parameters"), "type": "parameters" } );
            if (fragment !== "") args.append( { "pid": i-1, "parameter": fragment, "desc": qsTr("URI fragment"), "type": "fragment" } );
        }
    }

    /*
    // application settings:
    ConfigurationGroup  {
        id: settings
        path: "/org/nephros/" + Qt.application.name
    }
    ConfigurationGroup  {
        id: config
        scope: settings
        path:  "app"
        //property bool gravity: true // true: pull to bottom
        //property int ordering: 1 // 1: top to bottom
    }
    */

    initialPage: Component { MainPage{} }
    cover: CoverPage{}

}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
