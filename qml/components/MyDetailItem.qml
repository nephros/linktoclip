/*

Apache License 2.0

Copyright (c) 2022, 2023 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0


Column {
 property alias label: labelLabel.text
 property alias value: valueLabel.text
 property alias lalign: labelLabel.horizontalAlignment
 property alias valign: valueLabel.horizontalAlignment
 property var fontSize: Theme.fontSizeSmall
 Label { id: labelLabel ; width: parent.width; font.pixelSize: parent.fontSize; color: Theme.secondaryHighlightColor; wrapMode: Text.Wrap }
 Label { id: valueLabel ; width: parent.width; font.pixelSize: parent.fontSize; color: Theme.secondaryColor;          wrapMode: Text.WrapAnywhere }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
