/*

Apache License 2.0

Copyright (c) 2022, 2023 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    property bool autominimize: true

    function toClip(s) {
        Clipboard.text=s;
        popup(qsTr("Copied to Clipboard: %1").arg(s))
        if (autominimize) app.deactivate();
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: content.height
        PageHeader { id: header ; title: qsTr("Decomposed Parameters");}
        Column { id: topcol
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: header.bottom
            bottomPadding: Theme.paddingLarge
            //Label { text: qsTr("Tap an item to copy to Clipboard"); color: Theme.secondaryColor}
            //Separator{ width: parent.width; color: Theme.primaryColor ; horizontalAlignment: Qt.AlignHCenter}
            TextSwitch { id: minsw
                anchors.horizontalCenter: parent.horizontalCenter
                //anchors.top: header.bottom
                text: qsTr("Minimize app on tap")
                description: qsTr("If enabled, the app will put itself in the background as soon as an item has been selected.")
                checked: page.autominimize
                onClicked: page.autominimize = !page.autominimize
                Separator{ width: parent.width; color: Theme.primaryColor ; horizontalAlignment: Qt.AlignHCenter; anchors.verticalCenter: parent.bottom}
            }
            MyDetailItem {
                label: (Clipboard.text < 128) ? qsTr("Current Clipboard contents") : qsTr("Current Clipboard contents (truncated)")
                // if this makes height get too large, we can't see the flickable...
                value: (Clipboard.text < 128) ? Clipboard.text : ( Clipboard.text.substr(0,128) + "\n ...")
                lalign: Text.AlignHCenter
                valign: Text.AlignHCenter
                width: parent.width
                fontSize: Theme.fontSizeMedium
            }
            Separator{ width: parent.width; color: Theme.primaryColor ; horizontalAlignment: Qt.AlignHCenter}
        }
        SilicaListView { id: content
            width: parent.width - Theme.horizontalPageMargin
            height: page.height -  (header.height + topcol.height)
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: topcol.bottom
            spacing: Theme.paddingSmall
            add:      Transition { FadeAnimation { duration: 1200 } }
            move:     Transition { FadeAnimation { duration: 1200 } }
            populate: Transition { FadeAnimation { duration: 1200 } }
            clip: true
            section {
                property: "pid"
                delegate: SectionHeader { text: qsTr("Parameter #%1: ").arg(section)
                //Separator{ width: parent.width; color: Theme.primaryColor ; horizontalAlignment: Qt.AlignRight; anchors.verticalCenter: parent.top}
                }
            }
            model: args
            delegate: ListItem {
                visible: parameter.length > 0
                hidden: !visible
                onClicked: page.toClip(parm.value)
                contentHeight: parm.height
                MyDetailItem { id: parm ; label: desc; value: type === "parameters" ? parameter.split("&").join("\n") : parameter ; width: content.width }
                menu: (type != "uri")
                    ? undefined
                    : ctxmenu
                Component { id: ctxmenu
                    ContextMenu {
                        MenuItem { text: qsTr("Encode"); onClicked: parm.value = encodeURI(model.parameter) }
                        MenuItem { text: qsTr("Decode"); onClicked: parm.value = decodeURI(model.parameter) }
                        MenuItem { text: qsTr("Encode (full)"); onClicked: parm.value = encodeURIComponent(model.parameter) }
                        MenuItem { text: qsTr("Decode (full)"); onClicked: parm.value = decodeURIComponent(model.parameter) }
                }}
            }
        }
        PullDownMenu { id: pdp
            MenuItem { text: qsTr("About"); onClicked: { pageStack.push(Qt.resolvedUrl("AboutPage.qml")) } }
            //MenuItem { text: qsTr("Settings"); onClicked: { pageStack.push(Qt.resolvedUrl("SettingsPage.qml")) } }
            MenuItem { text: qsTr("Read Clipboard")
                onClicked: { 
                    args.clear();
                    app.handleParameterItem(app.parmstart, Clipboard.text)
                }
            }
        }
        VerticalScrollDecorator {}
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
