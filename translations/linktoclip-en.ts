<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="44"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>Copyright:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Source Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="33"/>
        <source>Copied to Clipboard: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="41"/>
        <source>Decomposed Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="59"/>
        <source>Current Clipboard contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="52"/>
        <source>Minimize app on tap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="53"/>
        <source>If enabled, the app will put itself in the background as soon as an item has been selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="78"/>
        <source>Parameter #%1: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="94"/>
        <source>Encode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="95"/>
        <source>Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="96"/>
        <source>Encode (full)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Decode (full)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="102"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="104"/>
        <source>Read Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>linktoclip</name>
    <message>
        <location filename="../qml/linktoclip.qml" line="61"/>
        <source>Original parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="72"/>
        <source>Without Protocol (scheme)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="74"/>
        <source>Server (local part)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="76"/>
        <source>URI string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="57"/>
        <source>Copied to Clipboard: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
