<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="44"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>What&apos;s %1?</source>
        <translation type="vanished">Was ist %1?</translation>
    </message>
    <message>
        <source> </source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>Copyright:</source>
        <translation>Urheberrecht:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>License:</source>
        <translation>Lizenz:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Source Code:</source>
        <translation>Quellcode:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Credits</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>Übersetzung: %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="33"/>
        <source>Copied to Clipboard: %1</source>
        <translation>In die Zwischenablage kopiert: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="41"/>
        <source>Decomposed Parameters</source>
        <translation>Aufgedröselte Parameter</translation>
    </message>
    <message>
        <source>Tap an item to copy to Clipboard</source>
        <translation type="vanished">Tippen zum kopieren in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="59"/>
        <source>Current Clipboard contents</source>
        <translation>Momentane Zwischenablage</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="52"/>
        <source>Minimize app on tap</source>
        <translation>App nach tippen minimieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="53"/>
        <source>If enabled, the app will put itself in the background as soon as an item has been selected.</source>
        <translation>Wenn eingeschalten mimimiert sich die app sobald ein Element angetippt wurde.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="78"/>
        <source>Parameter #%1: </source>
        <translation>Parameter Nr. %1: </translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="94"/>
        <source>Encode</source>
        <translation>Enkodieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="95"/>
        <source>Decode</source>
        <translation>Dekodieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="96"/>
        <source>Encode (full)</source>
        <translation>Enkodiere (alles)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Decode (full)</source>
        <translation>Dekodiere (alles)</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Minimieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="104"/>
        <source>Read Clipboard</source>
        <translation>Zwischanablage einlesen</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation type="vanished">Parameter</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="102"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>linktoclip</name>
    <message>
        <location filename="../qml/linktoclip.qml" line="61"/>
        <source>Original parameter</source>
        <translation>Original</translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="72"/>
        <source>Without Protocol (scheme)</source>
        <translation>ohne Protokoll (scheme)</translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="74"/>
        <source>Server (local part)</source>
        <translation>nur Server (local part)</translation>
    </message>
    <message>
        <source>Without protocol</source>
        <translation type="vanished">ohne Protokoll</translation>
    </message>
    <message>
        <source>Server only</source>
        <translation type="vanished">nur Server</translation>
    </message>
    <message>
        <source>Without Scheme</source>
        <translation type="vanished">ohne Schema</translation>
    </message>
    <message>
        <source>Server/local part only</source>
        <translation type="vanished">nur Server/local part</translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="76"/>
        <source>URI string</source>
        <translation>URI</translation>
    </message>
    <message>
        <source>URI string encoded</source>
        <translation type="vanished">URI enkodiert</translation>
    </message>
    <message>
        <source>URI string decoded</source>
        <translation type="vanished">URI dekodiert</translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="57"/>
        <source>Copied to Clipboard: %1</source>
        <translation>In die Zwischenablage kopiert: %1</translation>
    </message>
</context>
</TS>
