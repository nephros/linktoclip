<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="44"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source> </source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>Copyright:</source>
        <translation>Copyright:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>License:</source>
        <translation>Licens:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Source Code:</source>
        <translation>Källkod:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="50"/>
        <source>Credits</source>
        <translation>Erkännanden</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>Översättningar: %1</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="33"/>
        <source>Copied to Clipboard: %1</source>
        <translation>Kopierat till urklipp: %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="41"/>
        <source>Decomposed Parameters</source>
        <translation>Nedbrytbara parametrar</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="59"/>
        <source>Current Clipboard contents</source>
        <translation>Aktuellt urklippsinnehåll</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="52"/>
        <source>Minimize app on tap</source>
        <translation>Minimera appen vid snabbtryck</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="53"/>
        <source>If enabled, the app will put itself in the background as soon as an item has been selected.</source>
        <translation>Vid aktivering kommer appen att lägga sig i bakgrunden så snart ett objekt har valts.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="78"/>
        <source>Parameter #%1: </source>
        <translation>Parameter #%1: </translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="94"/>
        <source>Encode</source>
        <translation>Koda</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="95"/>
        <source>Decode</source>
        <translation>Avkoda</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="96"/>
        <source>Encode (full)</source>
        <translation>Koda (fullständig)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="97"/>
        <source>Decode (full)</source>
        <translation>Avkoda (fullständig)</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="102"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="104"/>
        <source>Read Clipboard</source>
        <translation>Läs urklipp</translation>
    </message>
</context>
<context>
    <name>linktoclip</name>
    <message>
        <location filename="../qml/linktoclip.qml" line="61"/>
        <source>Original parameter</source>
        <translation>Ursprunglig parameter</translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="72"/>
        <source>Without Protocol (scheme)</source>
        <translation>Utan protokoll (schema)</translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="74"/>
        <source>Server (local part)</source>
        <translation>Server (lokal del)</translation>
    </message>
    <message>
        <source>Without Scheme</source>
        <translation type="vanished">Utan protokoll</translation>
    </message>
    <message>
        <source>Server/local part only</source>
        <translation type="vanished">Endast server</translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="76"/>
        <source>URI string</source>
        <translation>URI-sträng</translation>
    </message>
    <message>
        <source>URI string encoded</source>
        <translation type="vanished">URI-sträng kodad</translation>
    </message>
    <message>
        <source>URI string decoded</source>
        <translation type="vanished">URI-sträng avkodad</translation>
    </message>
    <message>
        <location filename="../qml/linktoclip.qml" line="57"/>
        <source>Copied to Clipboard: %1</source>
        <translation>Kopierat till urklipp: %1</translation>
    </message>
</context>
</TS>
